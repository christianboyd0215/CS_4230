// parallelAstar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <concurrent_priority_queue.h>
#include <concurrent_unordered_map.h>
#include <concurrent_vector.h>
#include <string>
#include <tbb/task_scheduler_init.h>
#include <tbb/concurrent_vector.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_do.h>
#include <time.h>
//download tbb!
class Node {
public:Node()
	   {};
public:int id;
public:long x;
public:long y;
public:std::vector<int> edges;
public:long cost;
public:unsigned long gscore = 1000000000;
public:unsigned long fScore = 10000000000;
public:Node(const Node&other) { id = other.id; x = other.x; y = other.y; edges = other.edges; cost = other.cost; gscore = other.gscore; fScore = other.fScore; }
public:Node & operator=(const Node&) = default;
};

long heuristicEstimate(Node currentPos, Node endPos)
{
	long x = abs(currentPos.x - endPos.x);
	long y = abs(currentPos.y - endPos.y);
	//long z = currentPos.z - endPos.z;
	long result = x+y;
	return result;
}
concurrency::concurrent_vector<Node> graph;


void createGraph()
{
	int maxX = 1000;
	int maxY = 50;
	//int maxZ = 100;
	//auto beginning = graph.begin();
	for (int j = 0; j < maxY; j++)
	{
		for (int i = 0; i < maxX; i++)
		{

			Node theNode;
			theNode.x = i;
			theNode.y = j;
			//theNode.z = k;
			theNode.id = (i + maxX * j);// i + maxX * j + maxX * maxY*k
			theNode.cost = 1;
			theNode.fScore = INFINITY;
			if (i - 1 >= 0)
			{
				graph[(i - 1) + maxX * j].edges.push_back(theNode.id);
				theNode.edges.push_back(graph[(i - 1) + maxX * j].id);
			}
			if (j - 1 >= 0)
			{
				graph[(i)+maxX * (j - 1)].edges.push_back(theNode.id);
				theNode.edges.push_back(graph[(i)+maxX * (j - 1)].id);
			}
			// if (k - 1 > 0)
			// {
			//	 graph[(i) + maxX * j + maxX * maxY*(k-1)].edges.push_back(theNode);
			//	 theNode.edges.push_back(graph[(i) + maxX * j + maxX * maxY*(k-1)]);
			// }
			graph.push_back(theNode);

		}
	}
}
void createSmallGraph()
{
	Node a;
	Node b;
	Node c;
	Node d;
	a.x = 0;
	a.y = 0;
	b.x = 1;
	b.y = 0;
	c.x = 0;
	c.y = 1;
	d.x = 1;
	d.y = 1;
	a.cost = 0;
	b.cost = 0;
	c.cost = 0;
	d.cost = 0;
	a.id = 0;
	b.id = 1;
	c.id = 2;
	d.id = 3;
	a.edges.push_back(b.id);
	b.edges.push_back(a.id);
	a.edges.push_back(c.id);
	c.edges.push_back(a.id);

	d.edges.push_back(b.id);
	b.edges.push_back(d.id);
	d.edges.push_back(c.id);
	c.edges.push_back(d.id);

	graph.push_back(a);
	graph.push_back(b);
	graph.push_back(c);
	graph.push_back(d);

}
struct nodeComparison : public std::binary_function<Node, Node, bool>
{
	bool operator()(const Node& lhs, const Node& rhs) const
	{
		return lhs.fScore > rhs.fScore;
	}
};
struct dataPack {
	int current;
	concurrency::concurrent_priority_queue<Node, nodeComparison> *openSet;
	Concurrency::concurrent_vector<bool> *inOpenSet;
	Concurrency::concurrent_vector<bool> *closedSet;
	Concurrency::concurrent_vector<int> *cameFrom;
	int endpos;
	dataPack(int Current, concurrency::concurrent_priority_queue<Node, nodeComparison> *OpenSet,
		Concurrency::concurrent_vector<bool> *InOpenSet, Concurrency::concurrent_vector<bool> *ClosedSet,
		Concurrency::concurrent_vector<int> *CameFrom, int end)
	{
		current = Current;
		openSet = OpenSet;
		inOpenSet = InOpenSet;
		closedSet = ClosedSet;
		cameFrom = CameFrom;
		endpos = end;
	}
public:dataPack(const dataPack&other) { current = other.current; openSet = other.openSet; inOpenSet = other.inOpenSet; closedSet = other.closedSet; cameFrom = other.cameFrom; endpos = other.endpos; }
public:dataPack & operator=(const dataPack&) = default;

};
std::string reconstructedPath(concurrency::concurrent_vector<int> cameFrom, int current, int startpos)
{
	std::string totalPath = std::to_string(current);
	while (current != startpos)
	{
		current = cameFrom[current];
		if (current == -1)
		{
			return "path not found \n";
		}
		totalPath = std::to_string(current) + " " + totalPath;
	}
	totalPath += "\n";
	return totalPath;
}
std::string AstarSearch(int startpos, int endpos)
{
	concurrency::concurrent_vector<bool> closedSet;
	for (int i = 0; i < 50 * 1000; i++)
	{
		closedSet.push_back(false);
	}
	//concurrency::concurrent_unordered_map<int, Node>closedSet;
	concurrency::concurrent_vector<bool> inOpenSet;
	for (int i = 0; i < 50 * 1000; i++)
	{
		inOpenSet.push_back(false);
	}
	inOpenSet[startpos] = true;
	concurrency::concurrent_priority_queue < Node, nodeComparison> openSet;
	openSet.push(graph[startpos]);
	//std::vector<int>closedSet;
	concurrency::concurrent_vector<int> cameFrom;
	for (int i = 0; i < 50 * 1000; i++)
	{
		graph[i].gscore = 100000000;
		cameFrom.push_back(-1);
	}
	cameFrom[startpos] = startpos;
	
	long tmp = heuristicEstimate(graph[startpos], graph[endpos]);
	graph[startpos].gscore = 0;
	graph[startpos].fScore = tmp;
	// fScore.insert(std::make_pair(startpos, tmp));
	Node current;
	while (openSet.try_pop(current))
	{


		if (current.id == endpos)
		{
			return reconstructedPath(cameFrom, current.id, startpos);
		}
		closedSet[current.id] = true;
		for (int node : current.edges)
		{
			if (closedSet[node])
			{

			}
			else
			{
				//long tentative_g_score = gScore[current.id] + heuristicEstimate(current, graph[node]) + graph[node].cost;
				unsigned long tentative_g_score = current.gscore + heuristicEstimate(current, graph[node]) + graph[node].cost;
				if (tentative_g_score < graph[node].gscore)//graph[node].gscore
				{
					cameFrom[node] = current.id;
					graph[node].gscore = tentative_g_score;
					graph[node].fScore = tentative_g_score + heuristicEstimate(graph[node], graph[endpos]);
				}

				if (!inOpenSet[node])
				{
					openSet.push(graph[node]);
					inOpenSet[node] = true;
				}
			}


		}
	}
	return "no path found\n";
}

class Body {
	
public:Body() {
	}
	   void processNode(int current, concurrency::concurrent_priority_queue<Node, nodeComparison> *openSet,
		   Concurrency::concurrent_vector<bool> *inOpenSet, Concurrency::concurrent_vector<bool> *closedSet, Concurrency::concurrent_vector<int> *cameFrom, int endpos) const
	   {
		   closedSet->at(current) = true;
		   for (int node : graph[current].edges)
		   {
			   if (closedSet->at(node))
			   {

			   }
			   else
			   {
				   //long tentative_g_score = gScore[current.id] + heuristicEstimate(current, graph[node]) + graph[node].cost;
				   unsigned long tentative_g_score = graph[current].gscore + heuristicEstimate(graph[current], graph[node]) + graph[node].cost;
				   while (tentative_g_score < graph[node].gscore)//graph[node].gscore
				   {
					   unsigned long expected = graph[node].gscore;
					   std::_Atomic_thread_fence(std::memory_order_acquire);
					   //std::atomic_compare_exchange_strong()
					   if (std::atomic_compare_exchange_strong((std::atomic_ulong *)&graph[node].gscore, &expected, tentative_g_score))
					   {
						   cameFrom->at(node) = current;
						   //graph[node].gscore = tentative_g_score;
						   std::atomic_store((std::atomic_ulong *)&graph[node].fScore, tentative_g_score + heuristicEstimate(graph[node], graph[endpos]));
						   bool test = false;
						   if (std::atomic_compare_exchange_strong((std::atomic_bool*)&inOpenSet->at(node), &test, true))//should allow an atomic check and swap
						   {
							   openSet->push(graph[node]);
						   }
						   //graph[node].fScore = tentative_g_score + heuristicEstimate(graph[node], graph[endpos]);
					   }
					   std::_Atomic_thread_fence(std::memory_order_release);

				   }
				   //bool test = false;
				   
				   /*if (!inOpenSet[node])
				   {
				   inOpenSet->at(node) = true;
				   openSet->push(graph[node]);
				   }*/
			   }


		   }
	   }
public: void operator()(dataPack* item) const
{
	int current = item->current;
	Concurrency::concurrent_vector<int> *cameFrom = item->cameFrom;
	Concurrency::concurrent_vector<bool> *closedSet = item->closedSet;
	int endpos = item->endpos;
	Concurrency::concurrent_vector<bool> *inOpenSet = item->inOpenSet;
	concurrency::concurrent_priority_queue<Node, nodeComparison> *openSet = item->openSet;
	processNode(current, openSet, inOpenSet, closedSet, cameFrom, endpos);
}
};



 std::string runParallelAstar(int max_threads, int start, int end)
 {
	 concurrency::concurrent_vector<bool> closedSet;
	 for (int i = 0; i < 50 * 1000; i++)
	 {
		 closedSet.push_back(false);
	 }
	 //concurrency::concurrent_unordered_map<int, Node>closedSet;
	 concurrency::concurrent_vector<bool> inOpenSet;
	 for (int i = 0; i < 50 * 1000; i++)
	 {
		 inOpenSet.push_back(false);
	 }
	 inOpenSet[start] = true;
	 concurrency::concurrent_priority_queue < Node, nodeComparison> openSet;
	 openSet.push(graph[start]);
	 //std::vector<int>closedSet;
	 concurrency::concurrent_vector<int> cameFrom;
	 for (int i = 0; i < 50 * 1000; i++)
	 {
		 graph[i].gscore = 100000000;
		 cameFrom.push_back(-1);
	 }
	 cameFrom[start] = start;
	 graph[start].gscore = 0;
	 long tmp = heuristicEstimate(graph[start], graph[end]);
	 graph[start].fScore = tmp;
	 // fScore.insert(std::make_pair(startpos, tmp));
	 Node current;

	 /// Parallel section
	 std::vector<dataPack*> jobs;
	 int i = 0;
	 bool notdone = true;
	 bool innerloop = true;
	 while (notdone && openSet.try_pop(current))
	 {
	 //std::atomic_thread_fence(std::memory_order_release);
		 i = 0;
		 if (current.id == end)
		 {
			 notdone = false;
			 return reconstructedPath(cameFrom, current.id, start);
		 }
		 jobs.push_back(&dataPack(current.id, &openSet, &inOpenSet, &closedSet, &cameFrom, end));
		 if (notdone)
		 {
			 innerloop = true;
			 while (innerloop)
			 {
				 if (i < max_threads)
				 {
					 if (openSet.try_pop(current))
					 {
						 if (current.id == end)
						 {
							 notdone = false;
							 innerloop = false;
						 }
						 else
						 {
							 jobs.push_back(&dataPack(current.id, &openSet, &inOpenSet, &closedSet, &cameFrom, end));
							 i++;
						 }
					 }
					 else
					 {
						 innerloop = false;
					 }
				 }
				 else
				 {
					 innerloop = false;
				 }
			 }
		 }
		 
		 tbb::parallel_do(jobs.begin(), jobs.end(), Body());
	 //std::atomic_thread_fence(std::memory_order_acquire);
	 }
	 ///start weird case
	 ///notdone = true;

	 return reconstructedPath(cameFrom, end, start);
 }
 int main()
 {
	 createGraph();
	 // maze 1
	 graph[1035].cost = 100;
	 graph[1036].cost = 100;
	 graph[1034].cost = 100;
	 graph[2034].cost = 100;
	 graph[2036].cost = 100;
	 graph[2035].cost = 100;
	 graph[37].cost = 100;
	 graph[36].cost = 100;
	 graph[33].cost = 100;
	 graph[36].cost = 100;
	 graph[39].cost = 100;
	 graph[38].cost = 100;
	 graph[40].cost = 100;
	 graph[33].cost = 100;
	 graph[36].cost = 100;
	 graph[37].cost = 100;
	 graph[36].cost = 100;

	 graph[36].cost = 100;
	 graph[1031].cost = 100;
	 graph[1032].cost = 100;
	 // maze 2
	 graph[21834].cost = 100;
	 graph[21836].cost = 100;
	 graph[22834].cost = 100;
	 graph[22836].cost = 100;
	 graph[23834].cost = 100;
	 graph[23836].cost = 100;
	 graph[20836].cost = 100;
	 graph[20834].cost = 100;
	 graph[19834].cost = 100;
	 graph[19836].cost = 100;
	 //graph[19835].cost = 100;
	 graph[18837].cost = 100;
	 graph[18836].cost = 100;
	 graph[18833].cost = 100;
	 graph[18836].cost = 100;
	 graph[18839].cost = 100;
	 graph[18838].cost = 100;
	 graph[18840].cost = 100;
	 graph[18833].cost = 100;
	 graph[18836].cost = 100;
	 graph[18837].cost = 100;
	 graph[17835].cost = 100;

	 graph[836].cost = 100;
	 graph[1831].cost = 100;
	 graph[1832].cost = 100;
	 std::string theString;
	 int i = 0;
	 time_t start = time(0);
	 //for (i = 0; i < 100; i++) {
		// theString = AstarSearch(20835, 35);

	 //}
	 time_t end = time(0);
	 printf("%s\n", theString.c_str());
	 printf("%f\n", difftime(start, end)/100);

	 start = time(0);
	 //2 pops
	 for (i = 0; i < 100; i++) {
		 std::string theString = runParallelAstar(16, 20835, 35);
		 // printf("%s\n", theString.c_str());
	 }
	 end = time(0);
	 printf("%s\n", theString.c_str());
	 printf("%f\n", difftime(start, end) / 100);

	 //start = time(0);
	 ////4 pops
	 //for (i = 0; i < 100; i++) {
		// std::string theString = runParallelAstar(4, 20835, 35);
		// // printf("%s\n", theString.c_str());
	 //}
	 //end = time(0);
	 //printf("%s\n", theString.c_str());
	 //printf("%f\n", difftime(start, end) / 100);
	
	 //start = time(0);
	 ////4pops
	 //for (i = 0; i < 100; i++) {
		// std::string theString = runParallelAstar(4, 45, 35);
		// // printf("%s\n", theString.c_str());
	 //}
	 //end = time(0);
	 //printf("%s\n", theString.c_str());
	 //printf("%f\n", difftime(start, end) / 100);


	 
	 return 0;
 }
