#include <stdio.h>
//#include <sys/sysinfo.h>
//#include <pthread.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}

void serdp(RNG rng) {
  for(int i=rng.L; i<=rng.H; ++i)
  { C[i] = A[i] * B[i];}
  // for(int i=rng.L; i<=rng.H; ++i)
  // {
  //  printf("%d \n", C[i] );
  //}
  for(int i = rng.L+1; i<=rng.H;++i)
  { C[rng.L] = C[rng.L]+C[i];}
}

int pdpStatic(int thread_count, int size, int threshold) {
  RNG rng;
  int total = 0;
#pragma omp parallel num_threads(thread_count)
  {
#pragma omp for schedule(static)
      for(int i = 0; i<size; i++)
      {
        C[i] = A[i] * B[i];
      }
#pragma omp for schedule(static) reduction(+ : total )
      for(int i = 0; i<size; i++)
      {
        total += C[i];
      }
  }
  return total;
    // printf(" rng.L and rng.H are %d %d\n", rng.L, rng.H);
    /*
    RNG rngL = rng;
    RNG rngH = rng;

    rngL.H = rng.L + (rng.H - rng.L)/2;
    rngH.L = rngL.H+1;
// pthread_t tid1;
    //void (*pdp_ptr)(int) = pdp;
// pthread_create(&tid1, NULL, pdp, (void*)&rngL);
    // printf("--> creating thread for range %d %d\n", rngL.L, rngL.H);
    //pdp(rngL);
// pthread_t tid2;
    //printf("--> creating thread for range %d %d\n", rngH.L, rngH.H);
// pthread_create(&tid2, NULL, pdp, (void*)&rngH);
    // pdp(rngH);
// pthread_join(tid1,NULL);
// pthread_join(tid2, NULL);
    //printf(C);
    C[rng.L] = C[rngL.L]+C[rngH.L];
  }
    */
  return NULL;
}
int pdpDynamic(int thread_count, int size, int threshold) {
  RNG rng;
  int total = 0;
#pragma omp parallel num_threads(thread_count)
  {
#pragma omp for schedule(dynamic, threshold)
      for(int i = 0; i<size; i++)
      {
        C[i] = A[i] * B[i];
      }
      //printf("made A\n");
#pragma omp for schedule(dynamic,threshold) reduction(+ : total )
      for(int i = 0; i<size; i++)
      {
        total += C[i];
      }
      //printf("made B\n");
  }
  return total;
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main2()
{
  int Size =1024;
  int Threshold = 0;
    int thread_count = 0;
  double start, finish, elapsed;
  int u = 1;
  for(int j =1; j<=24;j++)
  {
    for(int k = 1; k<= Size; k = k*2)
    {

      thread_count = j;
      Threshold = k;
      //printf("this run uses %d threads and %d as te threshold\n", thread_count, Threshold);
  start = 0;
  finish = 0;
  elapsed = 0;
  /* printf("This system has                    \
          %d processors configured and\
          %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  Get_args(argc, argv);
  */
//  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  //printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    // printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }

  //RNG rng;
  //rng.L = 0;
  //rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  //printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //GET_TIME(start);
  struct timeval t;
  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  double result = 0;
  for(int i = 0; i<1000; i++)
  {
    result = pdpStatic(thread_count, Size, Threshold);
  }
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  finish = finish;
  //GET_TIME(finish);
  elapsed = (finish - start)/1000;
  printf("static(%d)= %e;\n",u, elapsed);

  free(A);
  free(B);
  free(C);

///////////
  start = 0;
  finish = 0;
  elapsed = 0;
  /* printf("This system has                    \
          %d processors configured and\
          %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  Get_args(argc, argv);
  */
  // printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  // printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    // printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }

  //RNG rng;
  //rng.L = 0;
  //rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  //printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //GET_TIME(start);

  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  result = 0;
  result = pdpDynamic(thread_count, Size, Threshold);
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  //GET_TIME(finish);
  elapsed = (finish - start)/1000;
  printf("dynamic(%d)= %e;\n",u, elapsed);
  u = u+1;
  free(A);
  free(B);
  free(C);
    }
  }
  return 0;
}

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  if (argc < 3) {
    main2();
    return 0;
  }
  int Size = strtol(argv[1], NULL, 10);
  int Threshold = strtol(argv[2], NULL, 10);
  int thread_count = strtol(argv[3], NULL, 10);
  double start, finish, elapsed;
  start = 0;
  finish = 0;
  elapsed = 0;
  /* printf("This system has                    \
          %d processors configured and\
          %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  Get_args(argc, argv);
  */
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  //printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    // printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }

  //RNG rng;
  //rng.L = 0;
  //rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  //printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //GET_TIME(start);
  struct timeval t;
  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  double result = 0;
  for(int i = 0; i<1000; i++)
  {
    result = pdpStatic(thread_count, Size, Threshold);
  }
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  finish = finish/1000;
  //GET_TIME(finish);
  elapsed = finish - start;
  printf("The elapsed time for static is %e seconds\n", elapsed);
  printf("Final C is\n");
  printf("%f\n", (double) result);

  free(A);
  free(B);
  free(C);

///////////
  start = 0;
  finish = 0;
  elapsed = 0;
  /* printf("This system has                    \
          %d processors configured and\
          %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  Get_args(argc, argv);
  */
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  // printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    // printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }

  //RNG rng;
  //rng.L = 0;
  //rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  //printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //GET_TIME(start);

  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  result = 0;
  result = pdpDynamic(thread_count, Size, Threshold);
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  //GET_TIME(finish);
  elapsed = finish - start;
  printf("The elapsed time for dynamic is %e seconds\n", elapsed);
  printf("Final C is\n");
  printf("%f\n", (double) result);

  free(A);
  free(B);
  free(C);
}
