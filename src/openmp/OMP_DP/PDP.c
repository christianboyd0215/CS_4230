#include <stdio.h>
//#include <sys/sysinfo.h>
//#include <pthread.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}

void serdp(RNG rng) {
  for(int i=rng.L; i<=rng.H; ++i)
  { C[i] = A[i] * B[i];}
  // for(int i=rng.L; i<=rng.H; ++i)
  // {
  //  printf("%d \n", C[i] );
  //}
  for(int i = rng.L+1; i<=rng.H;++i)
  { C[rng.L] = C[rng.L]+C[i];}
}

int pdpStatic(RNG* myrng) {
  RNG rng;
  int total = 0;
#pragma omp parallel num_threads(thread_count)
  {
#pragma omp for schedule(static) num_threads(thread_count)
    {
      for(int i = 0; i<myrng->H; i++)
      {
        C[i] = A[i] * B[i];
      }
    }
#pragma omp for reduction(+ : total )
    {
      for(int i = 0; i<myrng->H; i++)
      {
        total += C[i];
      }
    }
  }
  return total;
    // printf(" rng.L and rng.H are %d %d\n", rng.L, rng.H);
    /*
    RNG rngL = rng;
    RNG rngH = rng;

    rngL.H = rng.L + (rng.H - rng.L)/2;
    rngH.L = rngL.H+1;
// pthread_t tid1;
    //void (*pdp_ptr)(int) = pdp;
// pthread_create(&tid1, NULL, pdp, (void*)&rngL);
    // printf("--> creating thread for range %d %d\n", rngL.L, rngL.H);
    //pdp(rngL);
// pthread_t tid2;
    //printf("--> creating thread for range %d %d\n", rngH.L, rngH.H);
// pthread_create(&tid2, NULL, pdp, (void*)&rngH);
    // pdp(rngH);
// pthread_join(tid1,NULL);
// pthread_join(tid2, NULL);
    //printf(C);
    C[rng.L] = C[rngL.L]+C[rngH.L];
  }
    */
  return NULL;
}
int pdpDynamic(RNG* myrng) {
  RNG rng;
  int total = 0;
#pragma omp parallel num_threads(thread_count)
  {
#pragma omp for schedule(dynamic) num_threads(thread_count)
    {
      for(int i = 0; i<myrng->H; i++)
      {
        C[i] = A[i] * B[i];
      }
    }
#pragma omp for reduction(+ : total )
    {
      for(int i = 0; i<myrng->H; i++)
      {
        total += C[i];
      }
    }
  }
  return total;
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  double start, finish, elapsed;
  start = 0;
  finish = 0;
  elapsed = 0;
  printf("This system has\
          %d processors configured and\
          %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  Get_args(argc, argv);
  int Size = (int) pow(2, Exp);
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }

  RNG rng;
  rng.L = 0;
  rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //GET_TIME(start);
  struct timeval t;
  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  pdp(&rng);
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  //GET_TIME(finish);
  elapsed = finish - start;
  printf("The elapsed time is %e seconds\n", elapsed);
  printf("Final C is\n");
  printf("%f\n", (double) C[rng.L]);

  free(A);
  free(B);
  free(C);

  // optimization testing
  printf("Will do optimized DP of %d sized arrays\n", Size);
  Thres = Size /6;
  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }
  for(int i=0; i<Size; ++i) {
    printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }
  rng.L = 0;
  rng.H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  gettimeofday(&t, NULL);
  start = t.tv_sec + t.tv_usec/1000000.0;
  pdp(&rng);
  gettimeofday(&t, NULL);
  finish = t.tv_sec + t.tv_usec/1000000.0;
  elapsed = finish - start;
  printf("The elapsed time is %e seconds\n", elapsed);
  printf("Final C is\n");
  printf("%f\n", (double) C[rng.L]);

  free(A);
  free(B);
  free(C);
}
